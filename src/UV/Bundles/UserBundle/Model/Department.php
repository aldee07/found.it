<?php
/**
 * Filename: Department.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\UserBundle\Model;

/**
 * Department
 */
abstract class Department
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param $status
     * @return Department
     */
    public function setStatus($status)
    {
        !in_array($status, $this->getValidStatus()) ||
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Department
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get validStatuses
     *
     * @param bool $literal
     * @return array
     */
    public function getValidStatus($literal = false)
    {
        $data = [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ];

        return $literal ?
            $data : array_keys($data);
    }

    /**
     * @inheritdoc
     */
    public function getStatusMapLiteral()
    {
        return $this->getValidStatus(true);
    }

    /**
     * Lifecycle callback: prePersist
     */
    public function prePersist()
    {
        $this->createdAt ||
            $this->setCreatedAt(new \DateTime);
    }
}
