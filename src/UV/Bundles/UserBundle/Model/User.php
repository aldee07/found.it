<?php
/**
 * Filename: User.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\UserBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 */
abstract class User
{
    const TYPE_EMPLOYEE = 1;
    const TYPE_STUDENT  = 2;
    const TYPE_VISITOR = 3;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "6", max = "100", minMessage = "Minimum of 6 characters is required", maxMessage = "Maximum of 100 characters")
     * @var string
     */
    protected $username;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "7", max = "50", minMessage = "Minimum of 6 characters is required", maxMessage = "Maximum of 100 characters")
     * @Assert\Email(message = "Invalid email format", checkMX = true)
     * @var string
     */
    protected $email;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $avatar;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    protected $firstname;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    protected $lastname;

    /**
     * @var integer
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * @var \DateTime
     */
    protected $createdAt;
    
    /**
     * @var Department
     */
    protected $department;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * Set email
     * 
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    /**
     * Get email
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set status
     *
     * @param $status
     * @return User
     */
    public function setStatus($status)
    {
        !in_array($status, $this->getValidStatus()) ||
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set Department
     * 
     * @param Department $department
     * @return User
     */
    public function setDepartment(Department $department)
    {
        $this->department = $department;
        
        return $this;
    }
    
    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Get available types
     *
     * @return array
     */
    public function getAvailableTypes()
    {
        return [
            self::TYPE_EMPLOYEE => 'Employee',
            self::TYPE_STUDENT => 'Student',
            self::TYPE_VISITOR => 'Visitor',
        ];
    }

    /**
     * Alias of getAvailableTypes
     *
     * @return array
     */
    public function getTypeMapLiteral()
    {
        return $this->getAvailableTypes();
    }

    /**
     * Get validStatuses
     *
     * @param bool $literal
     * @return array
     */
    public function getValidStatus($literal = false)
    {
        $data = [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_BANNED => 'Banned',
        ];

        return $literal ? $data : array_keys($data);
    }

    /**
     * @inheritdoc
     */
    public function getStatusMapLiteral()
    {
        return $this->getValidStatus(true);
    }

    /**
     * Lifecycle callback: prePersist
     */
    public function prePersist()
    {
        $this->createdAt ||
            $this->setCreatedAt(new \DateTime);
    }
}
