/**
 * Created by: Aldee Mativo
 * Created on: 7/13/14 10:16 PM
 */

function FoundIt()
{
    this.alerts = {

        messagesPool : { warning : [], error : [], success : [] },
        messagesContainer: '#fi-alerts',

        isTypeValid : function(type) {
            return typeof this.messagesPool[type] != 'undefined';
        },

        addMessage : function(type, message) {
            if(this.isTypeValid(type)) {
                this.messagesPool[type].push(message);
            }

            this.resetView();

            return this
        },

        addError : function(message) { return this.addMessage('error', message); },

        addWarning : function(message) { return this.addMessage('warning', message); },

        addSuccess : function(message) { return this.addMessage('success', message); },

        clear : function(type) {
            if(typeof type != 'undefined') {
                if(this.isTypeValid(type)) {
                    this.messagesPool[type] = [];
                }
            } else {
                this.messagesPool.warning = [];
                this.messagesPool.error = [];
                this.messagesPool.success = [];
            }

            this.resetView();

            return this;
        },

        resetView : function() {
            var types = ['warning', 'error', 'success'];
            var messages = this.messagesPool;
            var html = '';

            for(var i = 0, len = types.length; i < len; i++) {
                var type = types[i],
                    title = type.charAt(0).toUpperCase() + type.slice(1),
                    text = '<div class="fi-alert ' + type + '">' +
                           '<img src="/bundles/uvbundlesfoundit/images/not_' + type + '.png" title="' + title + '" />' +
                           '<ul>'

                if(!messages[type].length) continue;

                for(var j in messages[type]) {
                    text += '<li>' + messages[type][j] + '</li>';
                }

                text += '</ul></div>';
                html += text;
            }

            $(this.messagesContainer).html(html);
        }
    };
}

FoundIt.prototype.loadModernUi = function() {
    $('#jqcarousel1, #jqcarousel2, #jqcarousel3, #jqcarousel4, #jqcarousel5, #jqcarousel6, #jqcarousel7').oneCarousel({
        easeIn: 'rotateIn',
        interval: 5000,
        pause: 'hover'
    });
};

FoundIt.prototype.injectSelectPlaceholder = function(sel, placeholder) {
    var target = $(sel + ' option').first();

    if(target.val() != '') {
        target = $('<option />')
        $(sel).prepend(target);
    }

    target.attr('disabled', 'disabled')
        .html(placeholder);

    return this;
};

FoundIt.prototype.loadTabs = function() {
    var switchers = '.fi-tabs > ul li',
        containers = '.fi-tabs > div';

    $(switchers).removeClass('active');
    $(containers).hide();

    $(switchers).click(function() {
        var idx = $(this).index();

        $(switchers).removeClass('active');
        $(this).addClass('active');

        $(containers).hide();
        $(containers).eq(idx).show();
    });

    $(switchers).first().addClass('active');
    $(containers).first().show();
};

FoundIt.prototype.loadSelect2 = function() {
    $('.select2').select2();
};

FoundIt.prototype.makeColorWheel = function(inputSel) {
    var i = $(inputSel);

    if(i.length) {
        var f = $.farbtastic('.fi-colorpicker'),
            color = '#008B8C';

        i.each(function () {
            f.linkTo(this);
        });

        !(i.val() && i.val() == 'none') || i.val('');
        i.val() || (i.val(color));

        i.keyup();

        $('#fi-colorpicker-reset').click(function() {
            i.val(color).keyup().val('none');
        });

        i.val() != color || i.val('none');
    }
};

FoundIt.prototype.loadSearch = function(options) {
    function search(key, mode) {
        $('#fi-ajax-loader > img').show();

        var isCurrentUser = typeof options.currentUser !== 'undefined';

        $.ajax({
            url: options.ajaxUrl,
            type: 'POST',
            data: { term: key, currentUser: isCurrentUser ? 1 : 0 },
            dataType: 'json',
            success: function(data) {
                if(typeof data.found !== 'undefined' && typeof data.lost !== 'undefined') {
                    var keys = ['found', 'lost'];
                    for(var i in keys) {
                        var i = keys[i],
                            container = $('.fi-item-' + i);

                        mode == 'append' || container.html('');

                        if(!data[i].length) {
                            $('.fi-item-' + i).html('<i>No item(s) found from the ' + i.toUpperCase()  + ' database.</i>');
                            continue;
                        }

                        for(var j = 0, k = data[i].length; j < k; j++) {
                            var d = data[i][j],
                                additionalLinks = '';


                            additionalLinks += typeof options.viewUrl === 'undefined' ? '' : ' | <a href="' + options.viewUrl.replace('/69', '') + '/' + d.id + '">View</a>';

                            if(isCurrentUser) {
                                if(d.status != parseInt(options.statuses.claimed)) {
                                    additionalLinks += typeof options.deleteUrl === 'undefined' ? '' : ' | <a href="' + options.deleteUrl.replace('/69', '') + '/' + d.id + '">Delete</a>';
                                    additionalLinks += typeof options.markAsClaimedUrl === 'undefined' ? '' : ' | <a href="' + options.markAsClaimedUrl.replace('/69', '') + '/' + d.id + '">Mark as claimed</a>';
                                } else {
                                    additionalLinks += ' | <span style="color: #e87352">This item has been claimed</span>';
                                }
                            }

                            var html = '<div class="toggle boxed fi-result-each">' +
                                '   <div class="toggle-header">' +
                                '       <a class="toggle-link collapsed" data-toggle="collapse" href="#toggle-' + i + '-' + d.id + '" hidefocus="true">' +
                                '           <h5>' +
                                '               <span class="' + d.legendClass + ' ' + i + '" title="' + d.legendTitle + '">' + i + '</span>' +
                                '               ' + d.title +
                                '           </h5>' +
                                '       </a>' +
                                '   </div>' +
                                '   <div id="toggle-' + i + '-' + d.id + '" class="toggle-body collapse" style="height: auto;">' +
                                '       <div class="inner">' +
                                '           <p>' +
                                '               ' + d.description + '<br /><br />' +
                                '               ' + d.date + additionalLinks +
                                '           </p>' +
                                '       </div>' +
                                '   </div>' +
                                '</div>';

                            container.append(html);
                        }
                    }
                }

                $('#fi-ajax-loader > img').hide();
            }
        });
    }

    // form submission event
    $(document.body).on('submit', '#fi-search-form', function(){
        var term = $(this).find('input').val();
        search(term, 'html');

        return false;
    });

    // form buttons: search
    $(document.body).on('click', '#fi-search-form .fi-xsearch', function() {
        $(this).parents('form').submit();
    });

    // form buttons: reset
    $(document.body).on('click', '#fi-search-form .fi-xclose', function() {
        $('#fi-search-form input').val('');
        $('#fi-search-form .fi-xsearch').click();
    });

    search('', 'append');
};