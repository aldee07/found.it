/**
 * Created by: Aldee Mativo
 * Created on: 7/13/14 11:20 PM
 */

var FI = new FoundIt();

FI.loadModernUi();
FI.loadTabs();
FI.loadSelect2();