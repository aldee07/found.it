<?php
/**
 * Filename: GeneratorCommand.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Form\Exception\InvalidArgumentException;

class GeneratorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('fi:generate:data')
            ->setDescription('Generates default/test dummy data.')
            ->addArgument('data', InputArgument::REQUIRED, 'What data to generate?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $validData = ['users', 'departments', 'reports'];
        $data = $input->getArgument('data');

        if(!in_array($data, $validData)) {
            throw new InvalidArgumentException('Argument given is invalid!');
        }

        $this->getContainer()
            ->get('uv.bundles.fi.service.data')
            ->run($output, $data);

    }
} 