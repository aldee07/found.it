<?php
/**
 * Filename: Utility.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

use UV\Bundles\FoundItBundle\Entity\User;

class Utility
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Add flash message
     *
     * @param $type
     * @param $message
     * @return Utility
     */
    public function addFlashMessage($type, $message)
    {
        $this->container
            ->get('session')
            ->getFlashBag()
            ->add($type, $message);

        return $this;
    }

    /**
     * Encodes given string to password format based on the encryption algorithm provided in the security.yml
     *
     * @param $password
     * @return mixed
     */
    public function encodePassword($password)
    {
        // encoding password
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user = new User());

        return $encoder->encodePassword($password, $user->getSalt());
    }
} 