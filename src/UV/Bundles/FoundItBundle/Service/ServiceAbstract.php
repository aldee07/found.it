<?php
/**
 * Filename: ServiceAbstract.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Service;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ServiceAbstract
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * Set container
     *
     * @param ContainerInterface $container
     * @return ServiceAbstract
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Set doctrine
     *
     * @param Registry $registry
     * @return ServiceAbstract
     */
    public function setDoctrine(Registry $registry)
    {
        $this->doctrine = $registry;

        return $this;
    }
} 