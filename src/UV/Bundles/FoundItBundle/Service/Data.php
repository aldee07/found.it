<?php
/**
 * Filename: Data.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Service;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

use UV\Bundles\FoundItBundle\Entity\Department;
use UV\Bundles\FoundItBundle\Entity\User;
use UV\Bundles\FoundItBundle\Entity\Actor;
use UV\Bundles\FoundItBundle\Entity\Item;
use UV\Bundles\FoundItBundle\Entity\Settlement;

class Data extends ServiceAbstract
{
    /**
     * @var string
     */
    protected $departmentsSource;

    /**
     * @var string
     */
    protected $usersSource;

    /**
     * Set departmentsSource
     *
     * @param string $source
     * @return Data
     */
    public function setDepartmentsSource($source)
    {
        $this->departmentsSource = $source;
        return $this;
    }

    /**
     * Set usersSource
     *
     * @param string $source
     * @return Data
     */
    public function setUsersSource($source)
    {
        $this->usersSource = $source;
        return $this;
    }

    public function run(OutputInterface $output, $data)
    {
        $output->writeln('Generating data for ' . $data);
        $root = $this->container->get('kernel')->getRootDir() . '/../web';

        $getContent = function($source) use ($root) {
            if($source && !empty($source))
            {
                $path = $root . $source;
                if(!file_exists($path))
                    throw new FileNotFoundException($source);

                $contents = file_get_contents($path);

                if(empty($contents))
                    throw new \Exception('The given file is empty.');

                return $contents;
            }
        };

        switch($data)
        {
            case 'departments':
                $content = $getContent($this->departmentsSource);
                $this->processDepartments($content);
                break;
            case 'users':
                $content = $getContent($this->usersSource);
                $this->processUsers($content);
                break;
            case 'reports':
                $this->processReports();
                break;
            default:
                throw new InvalidArgumentException('Invalid $data argument given!');
        }
    }

    protected function processDepartments($content)
    {
        $em = $this->doctrine->getManager();

        $crawler = new Crawler();
        $crawler->addContent($content);

        $departments = $crawler->filterXPath('//foundit/departments/department');
        foreach($departments as $department)
        {
            $crawler2 = new Crawler($department);

            $entity = new Department();
            $entity->setName($crawler2->filter('name')->text());

            $em->persist($entity);
        }

        $em->flush();
    }

    protected function processUsers($content)
    {
        $em = $this->doctrine->getManager();

        $crawler = new Crawler();
        $crawler->addContent($content);

        $typeMap = [
            'student' => User::TYPE_STUDENT,
            'employee' => User::TYPE_EMPLOYEE
        ];

        $users = $crawler->filterXPath('//foundit/users/user');
        $departmentObjects = [];

        $password = $this->container->get('uv.bundles.fi.service.utility')
            ->encodePassword('test');

        $crawl = function($mode = 'persist') use ($typeMap, $users, $em, &$departmentObjects, $password) {
            $departmentsCollection = [];
            foreach($users as $user)
            {
                $crawler2 = new Crawler($user);

                $departmentId = $crawler2->filter('department')->text();
                $departmentsCollection[] = $departmentId;

                if($mode != 'persist') continue;

                $username   = $crawler2->filter('username')->text();
                $email      = $crawler2->filter('email')->text();
                $firstname  = $crawler2->filter('firstname')->text();
                $lastname   = $crawler2->filter('lastname')->text();
                $type       = $crawler2->attr('type');
                $roles      = $crawler2->attr('role');


                $entity = new User();
                $entity->setUsername($username);
                $entity->setPassword($password);
                $entity->setEmail($email);
                $entity->setFirstname($firstname);
                $entity->setLastname($lastname);
                $entity->setDepartment($departmentObjects[$departmentId]);
                $entity->setType($typeMap[$type]);
                $entity->setRoles([$roles]);

                $em->persist($entity);
            }

            return $departmentsCollection;
        };

        $departmentIds = $crawl('departments');
        $departments = $this->doctrine
                    ->getRepository('UVBundlesFoundItBundle:Department')
                    ->findBy(['id' => $departmentIds]);
        if($departments)
        {
            foreach($departments as $dept) {
                $departmentObjects[$dept->getId()] = $dept;
            }
        }

        $crawl();
        $em->flush();
    }

    protected function processReports($lostCount = 300, $foundCount = 320)
    {
        $actorsPool = [];
        $users = $this->doctrine->getRepository('UVBundlesFoundItBundle:User')->findBy([
            'status' => User::STATUS_ACTIVE
        ]);

        $em = $this->doctrine->getManager();

        if($users) {
            $actorsPool = [Actor::TYPE_FINDER => [], Actor::TYPE_MISLAYER => []];

            foreach($users as $user) {
                foreach([Actor::TYPE_FINDER, Actor::TYPE_MISLAYER] as $actorType) {
                    $actor = new Actor();
                    $actor->setUser($user);
                    $actor->setType($actorType);

                    if(isset($actorsPool[$actorType][$user->getId()])) continue;
                    $actorsPool[$actorType][$user->getId()] = $actor;
                    $em->persist($actor);
                }
            }
        }

        $test = '';
        foreach($actorsPool as $type => $pool) {
            foreach($pool as $p) {
                $test .= $type . ':' . $p->getUser()->getId() . "\n";
            }
        }
        #exit($test);

        if(count($actorsPool)) {
            $pick = function(array $data) {
                shuffle($data);
                return end($data);
            };

            $randomize = function($data, $param = null) use ($actorsPool, $pick) {
                switch($data) {
                    case 'actor':
                        return !isset($actorsPool[$param]) ?: $pick($actorsPool[$param]);

                    case 'claim-days':
                        return $pick([
                            ['mon', 'tue', 'wed'], ['fri', 'sun', 'wed'], ['wed', 'thu'], ['mon', 'tue', 'wed', 'thu', 'fri', 'sat'], ['mon', 'tue', 'sat'],
                        ]);

                    case 'claim-time-start':
                        return $pick([ '06:00', '07:00', '08:00', '09:00', '10:00', '11:00' ]);

                    case 'claim-time-end':
                        return $pick([ '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00' ]);

                    case 'color':
                        return $pick([ '#ccc', '#fff', '#aaa', '#aaaccc', '#777', '#aaccff', '#000f78', '#999', '#222777', '#2277ff', '#808080', '#ff6647', '#606060', '#9f9f9f', '#000', '#f7f7f7' ]);

                    case 'contact-details':
                        return $pick([
                            'Mobile: 123, Email: a@test.com', 'Mobile: 132, Email: b@test.com', 'Mobile: 423, Email: c@test.com', 'Mobile: 125, Email: d@test.com', 'Mobile: 623, Email: e@test.com',
                            'Mobile: 213, Email: f@test.com', 'Mobile: 312, Email: g@test.com', 'Mobile: 143, Email: h@test.com', 'Mobile: 523, Email: i@test.com', 'Mobile: 127, Email: j@test.com',
                        ]);

                    case 'contact-person':
                        if(rand(0, 1)) {
                            return $pick([ 'Drow Ranger', 'Spirit Breaker', 'Shadow Shaman', 'Anti Mage', 'Sniper', 'Templar Assassin', 'Pudge', 'Wraith King', 'Tidehunter', 'Phantom Assassin' ]);
                        } elseif($param instanceof Actor) {
                            $user = $param->getUser();
                            return $user->getFirstname() . ' ' . $user->getLastname();
                        }
                        break;

                    case 'date':
                        $int = mt_rand(time() - 100000, time() - 500);
                        return date("Y-m-d H:i:s", $int);
                    case 'item':
                        return $pick([
                            'Nokia 3310 Cellphone', 'Razer Mouse', 'Umbrella', 'Java Programming book', 'Diablo III installer', 'Department uniform', 'Samsung Galaxy Duos phone', 'Study load',
                            'Gibson guitar', 'Project envelope', 'Apple earphones', 'Lacoste wallet', 'Apache ballpen', 'Lenovo Laptop', 'Sunglasses', 'GoPro', 'G-Shock', 'Selfie stick', 'Habagat Bag'
                        ]);
                    case 'location':
                        return $pick([
                            'Rm 301 Building A', 'Rm 302 Building A', 'Rm 303 Building A', 'Rm 304 Building A', 'Rm 305 Building A', 'Rm 306 Building A', 'Rm 307 Building A', 'Rm 308 Building A',
                            'Rm 301 Building B', 'Rm 302 Building B', 'Rm 303 Building B', 'Rm 304 Building B', 'Rm 305 Building B', 'Rm 306 Building B', 'Rm 307 Building B', 'Rm 308 Building B'
                        ]);
                        break;
                    case 'status':
                        return $pick([
                            Item::STATUS_UNCLAIMED, Item::STATUS_CLAIMED, Item::STATUS_UNCLAIMED, Item::STATUS_UNCLAIMED, Item::STATUS_UNCLAIMED, Item::STATUS_UNCLAIMED
                        ]);
                        break;
                    case 'string':
                        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        $randomString = '';
                        for ($i = 0; $i < $param; $i++) {
                            $randomString .= $characters[rand(0, strlen($characters) - 1)];
                        }
                        return $randomString;
                        break;
                    case 'urgency':
                        return $pick([ Item::URGENCY_LOW, Item::URGENCY_MEDIUM, Item::URGENCY_HIGH ]);
                        break;
                }
            };

            $generator = function($type, $count) use ($em, $randomize) {
                $actorTypeMap = [
                    Item::TYPE_LOST => Actor::TYPE_MISLAYER,
                    Item::TYPE_FOUND => Actor::TYPE_FINDER
                ];

                $settlementTypeMap = [
                    Item::TYPE_LOST => Settlement::TYPE_SURRENDER,
                    Item::TYPE_FOUND => Settlement::TYPE_CLAIM,
                ];
                for($i = 0; $i < $count; $i++) {
                    $actor = $randomize('actor', $actorTypeMap[$type]);

                    if(!$actor instanceof Actor) return;

                    $item = new Item();
                    $itemName = $randomize('item') . ' i' . rand(1, 400);

                    $item->setActor($actor)
                        ->setName($itemName)
                        ->setDescription($itemName . '. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
                        ->setColor($randomize('color'))
                        ->setLocation($randomize('location'))
                        ->setType($type)
                        ->setUrgency($randomize('urgency'))
                        ->setDate($randomize('date'))
                        ->setStatus($randomize('status'));

                    $settlement = new Settlement();
                    $settlement->setContactPerson($randomize('contact-person', $actor))
                        ->setContactDetails($randomize('contact-details'))
                        ->setClaimLocation($randomize('location'))
                        ->setClaimTimeStart($randomize('claim-time-start'))
                        ->setClaimTimeEnd($randomize('claim-time-end'))
                        ->setClaimDays($randomize('claim-days'))
                        ->setType($settlementTypeMap[$type])
                        ->setNote('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

                    $item->setSettlement($settlement);
                    $em->persist($item);
                }
            };

            $generator(Item::TYPE_LOST, $lostCount);
            $generator(Item::TYPE_FOUND, $foundCount);
            $em->flush();
        }
    }
}