<?php
/**
 * Filename: FormResponse.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Model;


class FormResponse 
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $redirectUrl = '';

    /**
     * Set type
     *
     * @param $type
     * @param $url
     * @return FormResponse
     */
    public function setType($type, $url = null)
    {
        $this->type = $type;
        $this->redirectUrl = $url;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set message
     *
     * @param $message
     * @return FormType
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get redirectUrl
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Set success
     *
     * @param $message
     * @param $url
     * @return FormType
     */
    public function setSuccess($message, $url = null)
    {
        $this->setType('success', $url)
            ->setMessage($message);

        return $this;
    }

    /**
     * Set warning
     *
     * @param $message
     * @param $url
     * @return FormType
     */
    public function setWarning($message, $url = null)
    {
        $this->setType('warning', $url)
            ->setMessage($message);

        return $this;
    }

    /**
     * Set Error
     *
     * @param $message
     * @param $url
     * @return FormType
     */
    public function setError($message, $url = null)
    {
        $this->setType('error', $url)
            ->setMessage($message);

        return $this;
    }
} 