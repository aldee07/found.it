<?php
/**
 * Filename: Settlement.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Settlement
 */
class Settlement
{
    const TYPE_SURRENDER = 0;
    const TYPE_CLAIM = 1;

    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "3", max = "255", minMessage = "Minimum of 3 characters is required", maxMessage = "Maximum of 100 characters")
     * @var string
     */
    private $contactPerson;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "6", minMessage = "Minimum of 6 characters is required")
     * @var string
     */
    private $contactDetails;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "6", minMessage = "Minimum of 6 characters is required")
     * @var string
     */
    private $claimLocation;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    private $claimTimeStart;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    private $claimTimeEnd;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var array
     */
    private $claimDays = [];

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $note;

    /**
     * @var Item
     */
    private $item;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return Settlement
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string 
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set contactDetails
     *
     * @param $contactDetails
     * @return Settlement
     */
    public function setContactDetails($contactDetails)
    {
        $this->contactDetails = $contactDetails;

        return $this;
    }

    /**
     * Get contactDetails
     *
     * @return string
     */
    public function getContactDetails()
    {
        return $this->contactDetails;
    }

    /**
     * Set claimLocation
     *
     * @param $claimLocation
     * @return Settlement
     */
    public function setClaimLocation($claimLocation)
    {
        $this->claimLocation = $claimLocation;

        return $this;
    }

    /**
     * Get claimLocation
     *
     * @return string
     */
    public function getClaimLocation()
    {
        return $this->claimLocation;
    }

    /**
     * Set claimTimeStart
     *
     * @param $claimTimeStart
     * @return Settlement
     */
    public function setClaimTimeStart($claimTimeStart)
    {
        $this->claimTimeStart = $claimTimeStart;

        return $this;
    }

    /**
     * Get claimTimeStart
     *
     * @return string
     */
    public function getClaimTimeStart()
    {
        return $this->claimTimeStart;
    }

    /**
     * Set claimTimeEnd
     *
     * @param $claimTimeEnd
     * @return Settlement
     */
    public function setClaimTimeEnd($claimTimeEnd)
    {
        $this->claimTimeEnd = $claimTimeEnd;

        return $this;
    }

    /**
     * Get claimTimeEnd
     *
     * @return string
     */
    public function getClaimTimeEnd()
    {
        return $this->claimTimeEnd;
    }
    
    /**
     * Set type
     *
     * @param integer $type
     * @return Settlement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set claimDays
     *
     * @param $claimDays
     * @return Settlement
     */
    public function setClaimDays($claimDays)
    {
        $this->claimDays = $claimDays;

        return $this;
    }

    /**
     * Get claimDays
     *
     * @return array
     */
    public function getClaimDays()
    {
        return $this->claimDays;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Settlement
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }
    
    /**
     * Set Item
     * 
     * @param Item $item
     * @return Settlement
     */
    public function setItem(Item $item)
    {
        $this->item = $item;
        
        return $this;
    }
    
    /**
     * Get Item
     * 
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
