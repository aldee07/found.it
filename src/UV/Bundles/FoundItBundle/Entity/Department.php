<?php
/**
 * Filename: Department.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Entity;

use UV\Bundles\UserBundle\Model\Department as BaseDepartment;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Department
 */
class Department extends BaseDepartment
{
    /**
     * @var ArrayCollection
     */
    private $users;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }
    
    /**
     * Add User
     *
     * @param User $user
     * @return Department
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove User
     *
     * @param User $user
     * @return Department
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
        
        return $this;
    }

    /**
     * Get Users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * toString
     *
     * @return string
     */
    public function __toString()
    {
        $name = $this->getName();
        return empty($name) ? 'Add Department' : $name;
    }
}
