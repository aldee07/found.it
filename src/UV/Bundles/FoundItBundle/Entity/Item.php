<?php
/**
 * Filename: Item.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Item
 */
class Item
{
    const TYPE_LOST = 0;
    const TYPE_FOUND = 1;

    const URGENCY_LOW = 0;
    const URGENCY_MEDIUM = 1;
    const URGENCY_HIGH = 2;

    const STATUS_UNCLAIMED = 0;
    const STATUS_CLAIMED = 1;
    const STATUS_DELETED = 2;

    /**
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "2", max = "255", minMessage = "Minimum of 6 characters is required", maxMessage = "Maximum of 100 characters")
     * @var string
     */
    private $name;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    private $description;
    
    /**
     * @Assert\Length(min = "6", max = "255", minMessage = "Minimum of 6 characters is required", maxMessage = "Maximum of 100 characters")
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $color;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @Assert\Length(min = "3", minMessage = "Minimum of 3 characters is required")
     * @var string
     */
    private $location;

    /**
     * @var integer
     */
    private $type;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var integer
     */
    private $urgency;

    /**
     * @Assert\NotBlank(message = "This field is required")
     * @var string
     */
    private $datetime;

    /**
     *
     * @var integer
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @var Actor
     */
    private $actor;
    
    /**
     * @Assert\Valid()
     * @var Settlement
     */
    private $settlement;

    /**
     * @var ArrayCollection
     */
    private $tags;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set image
     * 
     * @param string $image
     * @return Item
     */
    public function setImage($image)
    {
        $this->image = $image;
        
        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Item
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Item
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Item
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set urgency
     *
     * @param integer $urgency
     * @return Item
     */
    public function setUrgency($urgency)
    {
        $this->urgency = $urgency;

        return $this;
    }

    /**
     * Get urgency
     *
     * @return integer 
     */
    public function getUrgency()
    {
        return $this->urgency;
    }

    /**
     * Set date
     *
     * @param string $datetime
     * @return Item
     */
    public function setDate($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return string
     */
    public function getDate()
    {
        return $this->datetime;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Item
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Item
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set Actor
     * 
     * @param Actor $actor
     * @return Item
     */
    public function setActor(Actor $actor)
    {
        $this->actor = $actor;
        
        return $this;
    }
    
    /**
     * Get Actor
     * 
     * @return Actor
     */
    public function getActor()
    {
        return $this->actor;
    }
    
    /**
     * Set Settlement
     * 
     * @param Settlement $settlement
     * @return Item
     */
    public function setSettlement(Settlement $settlement)
    {
        $this->settlement = $settlement;
        
        return $this;
    }
    
    /**
     * Get Settlement
     * 
     * @return Settlement
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     * @return Item
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     * @return Item
     */
    public function removeItem(Tag $tag)
    {
        $this->tags->remove($tag);

        return $this;
    }

    /**
     * Get tags
     *
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get constants pairing/literal
     *
     * @param mixed $key
     * @return array
     */
    public function getConstants($key = null)
    {
        $pairing = [
            'urgency' => [
                self::URGENCY_LOW => 'Low',
                self::URGENCY_MEDIUM => 'Medium',
                self::URGENCY_HIGH => 'High',
            ],
            'type' => [
                self::TYPE_LOST => 'Lost',
                self::TYPE_FOUND => 'Found',
            ],
            'status' => [
                self::STATUS_UNCLAIMED => 'Unclaimed',
                self::STATUS_CLAIMED => 'Claimed',
                self::STATUS_DELETED => 'Removed',
            ]
        ];

        return isset($pairing[$key]) ? $pairing[$key] : $pairing;
    }

    /**
     * Doctrine: Lifecyclecallback: prePersist
     */
    public function prePersist()
    {
        $this->createdAt || $this->createdAt = new \DateTime();
        $this->status || $this->status = self::STATUS_UNCLAIMED;
    }
}
