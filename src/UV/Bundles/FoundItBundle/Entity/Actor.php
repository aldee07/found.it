<?php
/**
 * Filename: Actor.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Actor
 */
class Actor
{
    const TYPE_MISLAYER = 0;
    const TYPE_FINDER = 1;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var User
     */
    private $user;
    
    /**
     * @var ArrayCollection
     */
    private $items;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Actor
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set User
     * 
     * @param User $user
     * @return Actor
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        
        return $this;
    }
    
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Add Item
     *
     * @param Item $item
     * @return Actor
     */
    public function addItem(Item $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove Item
     *
     * @param Item $item
     * @return Actor
     */
    public function removeItem(Item $item)
    {
        $this->items->removeElement($item);
        
        return $this;
    }

    /**
     * Get Items
     *
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }
}
