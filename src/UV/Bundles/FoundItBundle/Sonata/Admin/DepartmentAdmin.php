<?php
/**
 * Filename: DepartmentAdmin.php
 * Author: Aldee Mativo
 * Date: 8/30/14 4:23 PM
 */

namespace UV\Bundles\FoundItBundle\Sonata\Admin;

use Sonata\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

use UV\Bundles\FoundItBundle\Entity\Department;

class DepartmentAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Department')
                ->add('name')
                ->add('status', 'choice', [ 'choices' => (new Department())->getStatusMapLiteral(), 'attr' => ['style' => 'min-width: 200px;'] ])
            ->end();
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('status', null, [ 'template' => 'SonataAdminBundle:Custom:base_list_status_field.html.twig' ]);
    }
} 