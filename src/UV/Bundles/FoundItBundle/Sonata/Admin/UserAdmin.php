<?php
/**
 * Filename: UserAdmin .php
 * Author: Aldee Mativo
 * Date: 8/30/14 4:23 PM
 */

namespace UV\Bundles\FoundItBundle\Sonata\Admin;

use Sonata\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

use UV\Bundles\FoundItBundle\Entity\User;

class UserAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {   
        $formMapper
            ->with('Account')
                ->add('username')
                ->add('email')
                ->add('password', null, [ 'data' => '' ])
            ->end()
            ->with('Info')
                ->add('firstname')
                ->add('lastname')
            ->end()
            ->with('Others')
                ->add('department')
                ->add('type', 'choice', [ 'choices' => (new User())->getTypeMapLiteral(), 'attr' => ['style' => 'min-width: 200px;'] ])
                ->add('status', 'choice', [ 'choices' => (new User())->getStatusMapLiteral(), 'attr' => ['style' => 'min-width: 200px;'] ])
            ->end();
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username')
                ->add('email')
                ->add('firstname')
                ->add('lastname')
                ->add('type', null, [ 'template' => 'SonataAdminBundle:Custom:base_list_type_field.html.twig' ])
                ->add('status', null, [ 'template' => 'SonataAdminBundle:Custom:base_list_status_field.html.twig' ]);
    }
    
    public function preUpdate($model)
    {
        $container = $this->getConfigurationPool()->getContainer();
        
        $model->setPassword($container->get('uv.bundles.fi.service.utility')
            ->encodePassword($model->getPassword()));
    }
} 