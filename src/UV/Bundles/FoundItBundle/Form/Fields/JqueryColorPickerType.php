<?php
/**
 * Filename: JqueryColorPickerType.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Form\Fields;

use Symfony\Component\Form\AbstractType;

class JqueryColorPickerType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'jquery_color_picker';
    }
} 