<?php
/**
 * Filename: ItemType.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use UV\Bundles\FoundItBundle\Entity\Actor;
use UV\Bundles\FoundItBundle\Entity\Item;
use UV\Bundles\FoundItBundle\Entity\Settlement;

class ItemType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $item = new Item();
        $settlementForm = $this->container->get('uv.bundles.fi.form.type.settlement_type');

        $builder
            ->add('name', null)
            ->add('description', 'textarea')
            ->add('urgency', 'choice', [
                'choices' => array_merge(['' => ''], $item->getConstants('urgency'))
            ])
            ->add('color', 'jquery_color_picker')
            ->add('location')
            ->add('date')
            ->add('settlement', $settlementForm)
            ->add('reportType', 'hidden', [
                'mapped' => false,
                'data' => Item::TYPE_LOST
            ])
        ;

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
            $reportType = (int) $event->getForm()->get('reportType')->getData();
            $reportMap = [
                Item::TYPE_LOST => [
                    'item' => Item::TYPE_LOST,
                    'actor' => Actor::TYPE_MISLAYER,
                    'settlement' => Settlement::TYPE_SURRENDER
                ],
                Item::TYPE_FOUND => [
                    'item' => Item::TYPE_FOUND,
                    'actor' => Actor::TYPE_FINDER,
                    'settlement' => Settlement::TYPE_CLAIM
                ]
            ];
            $type = $reportMap[$reportType];

            $item = $event->getData();

            $item->setType($type['item']);
            $item->getSettlement()->setType($type['settlement']);

            $item->getColor() !== '#008B8C' || $item->setColor(null);

            // check if actor exists
            $actorRepo = $this->repo('Actor');
            $actor = $actorRepo->findOneBy([
                'type' => $type['actor'],
                'user' => $this->getCurrentUser()
            ]);

            if(!$actor) {
                $actor = new Actor();
                $actor->setType($type['actor']);
                $actor->addItem($item);
                $actor->setUser($this->getCurrentUser());
            }

            $item->setActor($actor);

            $event->setData($item);
        });
    }

    /**
     * @inheritdoc
     */
    public function handleSubmission()
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if($form->isSubmitted()) {
            $response = $this->response;
            $response->setError('There were problem(s) submitting your report. Please review the fields below.');

            if($form->isValid()) {
                $item = $form->getData();

                $isUpdate = $item->getId() != null;

                $item->setStatus(Item::STATUS_UNCLAIMED);
                $this->persist($item)
                    ->flush();

                $response->setSuccess($isUpdate ? 'Report successfully updated.' : 'Your report was successfully published.',
                    $this->getRouter()->generate('uv_bundles_found_it_main_report_view', [ 'id' => $item->getId() ]
                ));
            } else {
                #var_dump($form->getErrors());exit;
            }

            $this->sendResponse();
        }
    }

    /**
     * @inheritdoc
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'UV\Bundles\FoundItBundle\Entity\Item'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'fi_form_item';
    }
} 