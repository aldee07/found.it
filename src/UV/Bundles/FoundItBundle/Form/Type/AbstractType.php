<?php
/**
 * Filename: AbstractType.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType as Base;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Doctrine\ORM\EntityManager;
use Doctrine\Bundle\DoctrineBundle\Registry;

use UV\Bundles\FoundItBundle\Model\FormResponse;

abstract class AbstractType extends Base
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var FormResponse
     */
    protected $response;

    /**
     * Handles form submission
     */
    abstract public function handleSubmission();

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->request = $container->get('request');
        $this->router = $this->container->get('router');

        $this->doctrine = $container->get('doctrine');
        $this->em = $this->doctrine->getManager();

        $this->response = new FormResponse();
    }

    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param mixed $data The initial data for the form
     * @param array $options Options for the form
     *
     * @return Form
     */
    public function create($data = null, array $options = [])
    {
        $form = $this->container->get('form.factory')->create($this->getName(), $data, $options);
        $this->setForm($form);

        $this->handleSubmission();

        return $form;
    }

    /**
     * Set form
     *
     * @param Form $form
     * @return AbstractType
     */
    protected function setForm(Form $form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return Form
     */
    protected function getForm()
    {
        return $this->form;
    }

    /**
     * Returns the service
     *
     * @param $serviceName
     * @return mixed
     */
    protected function get($serviceName)
    {
        return $this->container->get($serviceName);
    }

    /**
     * Get repository
     *
     * @param string $entity
     * @param string $prefix
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function repo($entity, $prefix = 'UVBundlesFoundItBundle:')
    {
        return $this->doctrine->getRepository($prefix . $entity);
    }

    /**
     * Persists entity
     *
     * @param $entity
     * @return AbstractType
     */
    protected function persist($entity)
    {
        $this->em->persist($entity);

        return $this;
    }

    /**
     * Flushes em
     *
     * @return AbstractType
     */
    protected function flush()
    {
        $this->em->flush();

        return $this;
    }

    /**
     * Get router
     *
     * @return object|Router
     */
    protected function getRouter()
    {
        return $this->router;
    }

    /**
     * Returns a RedirectResponse to the given URL.
     *
     * @param string $url The URL to redirect to
     * @param int $status The status code to use for the Response
     *
     * @return RedirectResponse
     */
    protected function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Sends response (flash messages) and redirects if applicable
     *
     * @param bool $autoRedirect
     * @return mixed|AbstractType
     */
    public function sendResponse($autoRedirect = true)
    {
        $response = $this->response;
        $this->get('uv.bundles.fi.service.utility')
            ->addFlashMessage($response->getType(), $response->getMessage());

        if($autoRedirect && $response->getRedirectUrl()) {
            header('Location: ' . $response->getRedirectUrl());
            exit;
        }

        return $this;
    }

    /**
     * Get current user logged in
     *
     * @return mixed | NULL if not logged in
     */
    public function getCurrentUser()
    {
        $token = $this->container->get('security.context')->getToken();

        return !$token ?: $token->getUser();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'fi_form_' . time();
    }
} 