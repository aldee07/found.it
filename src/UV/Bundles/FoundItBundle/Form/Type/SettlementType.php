<?php
/**
 * Filename: SettlementType.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use UV\Bundles\FoundItBundle\Entity\Settlement;

class SettlementType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contactPerson')
            ->add('contactDetails', 'textarea')
            ->add('claimLocation', 'textarea')
            ->add('claimTimeStart')
            ->add('claimTimeEnd')
            ->add('claimDays', 'choice', [
                'multiple' => true,
                'choices' => [
                    'sun' => 'Sunday',
                    'mon' => 'Monday',
                    'tue' => 'Tuesday',
                    'wed' => 'Wednesday',
                    'thu' => 'Thursday',
                    'fri' => 'Friday',
                    'sat' => 'Saturday',
                ]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function handleSubmission()
    {

    }

    /**
     * @inheritdoc
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'UV\Bundles\FoundItBundle\Entity\Settlement'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'fi_form_settlement';
    }
} 