<?php
/**
 * Filename: SignupType.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use UV\Bundles\FoundItBundle\Entity\User;

class SignupType  extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null)
            ->add('email', 'email')
            ->add('password', 'password')
            ->add('firstname', null)
            ->add('lastname', null)
            ->add('type', 'choice', [
                'choices' => array_merge(['' => ''], (new User())->getAvailableTypes())
            ])
            ->add('department', null)
        ;
    }

    /**
     * @inheritdoc
     */
    public function handleSubmission()
    {
        $form = $this->getForm();
        $form->handleRequest($this->request);

        if($form->isSubmitted()) {
            $response = $this->response;
            $response->setError('Please correct the errors below');

            if($form->isValid()) {
                $userRepo = $this->repo('User');

                $username = $form['username']->getData();
                $email = $form['email']->getData();

                $response->setError('Either username/email is already taken. Please try another one.');

                if(!$userRepo->identityExists($username, $email)) {
                    $data = $form->getData();
                    $data->setPassword($this->get('uv.bundles.fi.service.utility')
                        ->encodePassword($data->getPassword()));

                    $this->em->persist($data);
                    $this->em->flush();

                    $response->setSuccess(
                        'Thank you for your registration. <a href="' . $this->getRouter()->generate('uv_bundles_found_it_main_auth') . '">Sign in here</a>',
                        $this->getRouter()->generate('uv_bundles_found_it_main_home')
                    );

                }
            }

            $this->sendResponse();
        }
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'fi_form_signup';
    }
} 