<?php
/**
 * Filename: SearchController.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use UV\Bundles\FoundItBundle\Entity\Item;

class SearchController extends Controller
{
    public function searchAction()
    {
        return $this->render('UVBundlesFoundItBundle:Pages:search/search.html.twig');
    }

    public function searchAjaxAction(Request $request)
    {
        $itemRepository = $this->getDoctrine()->getRepository('UVBundlesFoundItBundle:Item');

        $term = $request->get('term');
        $currentUser = (int) $request->get('currentUser');

        if($currentUser) {
            $user = $this->get('security.context')->getToken()->getUser();
            $itemRepository->setReporter($user);
        }

        $items = $itemRepository->getRecentItems($term, 10000);

        $data = ['lost' => [], 'found' => []];
        $legendMap = [
            Item::URGENCY_LOW => ['class' => 'low', 'title' => 'Low claim urgency level'],
            Item::URGENCY_MEDIUM => ['class' => 'medium', 'title' => 'Medium claim urgency level'],
            Item::URGENCY_HIGH => ['class' => 'high', 'title' => 'High claim urgency level - must be claimed immediately'],
        ];

        foreach($items as $type => $items) {
            if($items && count($items)) {
                foreach($items as $item) {
                    if($item instanceof Item) {
                        $data[$type][] = [
                            'id' => $item->getId(),
                            'title' => $item->getName(),
                            'status' => $item->getStatus(),
                            'legendTitle' => $legendMap[$item->getUrgency()]['title'],
                            'legendClass' => $legendMap[$item->getUrgency()]['class'],
                            'description' => $item->getDescription(),
                            'date' => $item->getCreatedAt()->format('d M Y H:i')
                        ];
                    }
                }
            }
        }

        return new JsonResponse($data);
    }
}