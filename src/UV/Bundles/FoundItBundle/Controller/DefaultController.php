<?php
/**
 * Filename: DefaultController.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use UV\Bundles\FoundItBundle\Entity\Item;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UVBundlesFoundItBundle:Pages:main/index.html.twig');
    }

    public function homeAction()
    {
        $itemRepository = $this->getDoctrine()->getRepository('UVBundlesFoundItBundle:Item');
        $recentFound = $itemRepository->getRecentFoundItems(7);
        $recentLost = $itemRepository->getRecentLostItems(7);

        return $this->render('UVBundlesFoundItBundle:Pages:main/home.html.twig', [
            'fi' => [
                'found' => $recentFound,
                'lost' => $recentLost,
                'urgencyMap' => [
                    Item::URGENCY_LOW => ['class' => 'low', 'title' => 'Low claim urgency level'],
                    Item::URGENCY_MEDIUM => ['class' => 'medium', 'title' => 'Medium claim urgency level'],
                    Item::URGENCY_HIGH => ['class' => 'high', 'title' => 'High claim urgency level - must be claimed immediately'],
                ]
            ]
        ]);
    }
    
    public function aboutAction()
    {
        return $this->render('UVBundlesFoundItBundle:Pages:main/about.html.twig');
    }
    
    public function contactAction()
    {
        return $this->render('UVBundlesFoundItBundle:Pages:main/contact.html.twig');
    }
    
    public function sitemapAction()
    {
        return $this->render('UVBundlesFoundItBundle:Pages:main/index.html.twig');
    }
}
