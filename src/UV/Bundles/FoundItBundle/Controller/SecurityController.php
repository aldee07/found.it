<?php
/**
 * Filename: SecurityController.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

use UV\Bundles\FoundItBundle\Entity\User;

class SecurityController extends Controller
{
    /**
     * Registration Page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction()
    {
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('uv_bundles_found_it_main_home'));
        }

        $form = $this->get('uv.bundles.fi.form.type.signup_type')->create(new User());

        return $this->render('UVBundlesFoundItBundle:Pages:security/register.html.twig', [
            'fi' => [ 'form' => $form->createView() ]
        ]);
    }

    /**
     * Login Page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('uv_bundles_found_it_main_home'));
        }

        $session = $request->getSession();

        $hasError = ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) ||
            (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR));

        !$hasError || $this->get('uv.bundles.fi.service.utility')->addFlashMessage('error', 'Invalid username/password.');

        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render('UVBundlesFoundItBundle:Pages:security/login.html.twig', [ 'last_username' => $lastUsername ]);
    }

    /**
     * Forgot Password Page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forgotPasswordAction(Request $request)
    {
        $this->get('uv.bundles.fi.service.utility')->addFlashMessage('success', 'Thank you for your registration. Click <a href="' . $this->generateUrl('uv_bundles_found_it_main_auth') . '">here</a> to sign in.');

        return $this->render('UVBundlesFoundItBundle:Pages:main/index.html.twig');
    }
}
