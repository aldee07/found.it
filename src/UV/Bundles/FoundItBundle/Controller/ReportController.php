<?php
/**
 * Filename: ReportController.php
 * Author: Aldee Mativo
 * Date: 7/15/14 12:10 AM
 */

namespace UV\Bundles\FoundItBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use UV\Bundles\FoundItBundle\Entity\Item;
use UV\Bundles\FoundItBundle\Form\Type\ItemType;

class ReportController extends Controller
{
    protected function getLabels($purpose)
    {
        $labels = [
            Item::TYPE_FOUND => [
                'header' => 'Report found item',

                'tab1_title' => 'What did you found?',
                'tab3_title' => 'Where to claim?',

                'tab1_header' => 'Please provide some helpful details of the item you found.',
                'tab1_label_itemName' => 'What did you found?',
                'tab1_label_itemUrgency' => 'How urgently it should be claimed?',

                'tab2_header' => 'Where and when did you found it?',
                'tab2_label_location' => 'Where exactly did you found it? or maybe describe the location',
                'tab2_label_dateTime' => 'When approximately did you have it?',

                'tab3_header' => 'Where to claim?',
                'tab3_label_settlementLocation' => 'Claim location'
            ],
            Item::TYPE_LOST => [
                'header' => 'Report lost item',

                'tab1_title' => 'What did you lost?',
                'tab3_title' => 'Where to surrender?',

                'tab1_header' => 'Please provide some helpful details of the item you lost.',
                'tab1_label_itemName' => 'What did you lost?',
                'tab1_label_itemUrgency' => 'How urgently it should be surrendered?',

                'tab2_header' => 'Where and when did you lost it?',
                'tab2_label_location' => 'Where exactly did you lost it? or maybe describe the location',
                'tab2_label_dateTime' => 'When approximately did you lost it?',

                'tab3_header' => 'Where to surrender?',
                'tab3_label_settlementLocation' => 'Surrender location'
            ],
        ];

        return !isset($labels[$purpose]) ?: $labels[$purpose];
    }

    protected function buildReport(Request $request, $reportType, Item $item = null)
    {
        $form = $this->get('uv.bundles.fi.form.type.item_type')->create($item ? $item : new Item());
        $request->isMethod('POST') || $form->get('reportType')->setData($reportType);

        $typeMap = [
            Item::TYPE_LOST => 'lost',
            Item::TYPE_FOUND => 'found',
        ];

        if(!$item && !$request->isMethod('post')) {
            $user = $this->get('security.context')->getToken()->getUser();

            $form->get('settlement')
                ->get('contactPerson')
                ->setData($user->getFirstname() . ' ' . $user->getLastname());

            $form->get('settlement')
                ->get('contactDetails')
                ->setData("mobile: \nemail: " . $user->getEmail() . "\n");
        }

        return $this->render('UVBundlesFoundItBundle:Pages:reports/report.html.twig', [
            'fi' => [
                'form' => $form->createView(),
                'labels' => $this->getLabels($reportType),
                'reportType' => $typeMap[$reportType],
                'isUpdate' => $item != null
            ]
        ]);
    }

    public function lostAction(Request $request)
    {
        return $this->buildReport($request, Item::TYPE_LOST);
    }
    
    public function foundAction(Request $request)
    {
        return $this->buildReport($request, Item::TYPE_FOUND);
    }

    public function myReportsAction(Request $request)
    {
        return $this->render('UVBundlesFoundItBundle:Pages:reports/my-reports.html.twig', [
            'fi' => [
                'statusDeleted' => Item::STATUS_DELETED,
                'statusClaimed' => Item::STATUS_CLAIMED,
            ]
        ]);
    }

    public function updateReportAction(Request $request)
    {
        $id = $request->get('id');
        $itemRepository = $this->getDoctrine()->getRepository('UVBundlesFoundItBundle:Item');

        $item = $itemRepository->find($id);

        if(!($item instanceof Item) || $item->getStatus() != Item::STATUS_UNCLAIMED) {
            $this->get('uv.bundles.fi.service.utility')->addFlashMessage('error', 'The resource/report you requested does not exist.');
            return $this->redirect($this->generateUrl('uv_bundles_found_it_main_home'));
        }

        return $item->getActor()->getUser() != $this->get('security.context')->getToken()->getUser() ?
            $this->redirect($this->generateUrl('uv_bundles_found_it_main_report_my_reports')) :
            $this->buildReport($request, $item->getType(), $item);
    }

    protected function updateItemStatus(Request $request, $mode = 'delete')
    {
        $variablesMap = [
            'delete' => [
                'status' => Item::STATUS_DELETED,
                'header' => 'Delete Report',
                'question' => ' This action cannot be undone. Are you sure you want to delete this report?',
                'message' => 'Item report successfully deleted.'
            ],
            'mark' => [
                'status' => Item::STATUS_CLAIMED,
                'header' => 'Update Report',
                'question' => ' This action cannot be undone. Are you sure you want to mark this item report as claimed?',
                'message' => 'Item report successfully marked as claimed.'
            ]
        ];

        $id = $request->get('id');
        $itemRepository = $this->getDoctrine()->getRepository('UVBundlesFoundItBundle:Item');

        if($request->isMethod('post')) {
            $itemRepository->find($id)
                ->setStatus($variablesMap[$mode]['status']);

            $this->getDoctrine()->getManager()->flush();

            $this->get('uv.bundles.fi.service.utility')->addFlashMessage('success', $variablesMap[$mode]['message']);
            return $this->redirect($this->generateUrl('uv_bundles_found_it_main_report_my_reports'));
        }

        $item = $itemRepository->find($id);

        !($item && $item->getActor()->getUser() != $this->get('security.context')->getToken()->getUser()) || $item = false;

        return !$item ?
            $this->redirect($this->generateUrl('uv_bundles_found_it_main_report_my_reports')) :
            $this->render('UVBundlesFoundItBundle:Pages:reports/update-status.html.twig', [
                'fi' => [
                    'item' => $item,
                    'header' => $variablesMap[$mode]['header'],
                    'question' => $variablesMap[$mode]['question'],
                    'modeMap' => [
                        Item::TYPE_FOUND => 'Found item',
                        Item::TYPE_LOST => 'Lost item',
                    ]
                ]
            ]);
    }

    public function deleteReportAction(Request $request)
    {
        return $this->updateItemStatus($request, 'delete');
    }

    public function markReportAction(Request $request)
    {
        return $this->updateItemStatus($request, 'mark');
    }

    public function reportViewAction(Request $request, $id)
    {
        $itemRepository = $this->getDoctrine()->getRepository('UVBundlesFoundItBundle:Item');

        $item  = $itemRepository->find($id);

        if(!$item || $item->getStatus() == Item::STATUS_DELETED) {
            $this->get('uv.bundles.fi.service.utility')->addFlashMessage('error', 'The resource/report you requested does not exist.');
            return $this->redirect($this->generateUrl('uv_bundles_found_it_main_search'));
        }

        $item->getStatus() != Item::STATUS_CLAIMED ||
            $this->get('uv.bundles.fi.service.utility')->addFlashMessage('warning', 'This report/item has been claimed.');


        return $this->render('UVBundlesFoundItBundle:Pages:reports/view.html.twig', [
            'fi' => [
                'item' => $item,
                'isOwner' => $this->get('security.context')->getToken()->getUser() == $item->getActor()->getUser(),
                'modeMap' => $item->getConstants() + [
                    'turnover' => [
                        Item::TYPE_LOST => 'Surrender',
                        Item::TYPE_FOUND => 'Claim'
                    ],
                    'days' => [
                        'sun' => 'Sunday',
                        'mon' => 'Monday',
                        'tue' => 'Tuesday',
                        'wed' => 'Wednesday',
                        'thu' => 'Thursday',
                        'fri' => 'Friday',
                        'sat' => 'Saturday'
                    ],
                    'claimedStatusValue' => Item::STATUS_CLAIMED
                ]
            ]
        ]);
    }
}
