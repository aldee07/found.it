<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

date_default_timezone_set('Asia/Manila');

$loader = require_once __DIR__.'/../var/bootstrap.php.cache';
Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', false);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
