@echo off

set db_user=root
set db_name=foundit
set db_pass=admin
set db_host=127.0.0.1

echo -------------------------------------------
echo --- FoundIt Data Generator ^| Aldee Labs ---
echo -------------------------------------------

mysql -u%db_user% -p%db_pass% -e "DROP SCHEMA IF EXISTS %db_name%; CREATE SCHEMA %db_name%; USE %db_name%"

php console doctrine:schema:update --force
php console fi:generate:data departments
php console fi:generate:data users
php console fi:generate:data reports

echo -------------------------------------------
echo ---        Generation Completed!        ---
echo -------------------------------------------

pause